package com.example.homework4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.account_layout.view.*

class Account(private val model:UserModel.Data, private val activity: MainActivity):RecyclerView.Adapter<Account.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_layout, parent, false))
    }

    override fun getItemCount() = 1;

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind();
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            Glide.with(activity).load(model.avatar).into(itemView.AccountImageView)
            itemView.AccountIdTextView.text = model.id.toString();
            itemView.AccountNameTextView.text = model.firstName;
            itemView.AccountLastNameTextView.text = model.lastName;
            itemView.AccountMailTextView.text = model.email;
        }
    }
}